# War of Life

War of Life - Adapted Game of Life version based on a tutorial by Spicy Yoghurt

Adapted it in a way to have two competing colonies (blue and red) fighting for available space.

Viewable in browser. Refresh to restart simulation.


Saying thank you to and credits go to: 

1. This is an altered Game of Life sim based on a tutorial by Spicy Yoghurt. Visit [spicyyoghurt.com](https://spicyyoghurt.com) for more tutorials.

2. Using very basic implementation of go-ozzo/ozzo-routing for serving the index file containing the content. Content page is HTML / JavaScript. Visit [go-ozzo on github](https://github.com/go-ozzo).