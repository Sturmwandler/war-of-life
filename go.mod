module war-of-life

go 1.18

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-ozzo/ozzo-routing v2.1.4+incompatible // indirect
	github.com/golang/gddo v0.0.0-20210115222349-20d68f94ee1f // indirect
	github.com/stretchr/testify v1.7.5 // indirect
)
